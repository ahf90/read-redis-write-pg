import os
from sqlalchemy import Column, Integer, String, Date, DateTime, Time
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine, MetaData

Base = declarative_base()

DESIRED_TABLES = ['game']

SQL_DB_TYPE = os.getenv('SQL_DB_TYPE', 'postgresql')
SQL_DB_USERNAME = os.getenv('SQL_DB_USERNAME')
SQL_DB_PASS = os.getenv('SQL_DB_PASS')
SQL_SERVICE_HOST = os.getenv('SQL_SERVICE_HOST')
SQL_SERVICE_PORT = os.getenv('SQL_SERVICE_PORT', 5432)


class Game(Base):
    __tablename__ = 'game'
    gameId = Column(Integer, primary_key=True)
    awayConference = Column(String, nullable=True)
    awayConferenceId = Column(Integer, nullable=True)
    awayDivision = Column(String, nullable=True)
    awayDivisionId = Column(Integer, nullable=True)
    awayTeamAbbr = Column(String, nullable=True)
    awayTeamId = Column(Integer, nullable=True)
    date = Column(Date, nullable=False)
    datetime = Column(DateTime, nullable=True)
    datetimeUtc = Column(DateTime, nullable=True)
    gameStatus = Column(String, nullable=False)
    gameStatusId = Column(Integer, nullable=False)
    gameType = Column(String, nullable=False)
    gameTypeId = Column(Integer, nullable=False)
    homeConference = Column(String, nullable=True)
    homeConferenceId = Column(Integer, nullable=True)
    homeDivision = Column(String, nullable=True)
    homeDivisionId = Column(Integer, nullable=True)
    homeTeamAbbr = Column(String, nullable=True)
    homeTeamId = Column(Integer, nullable=True)
    season = Column(Integer, nullable=False)
    sport = Column(String, nullable=False)
    sportId = Column(Integer, nullable=False)
    timeEst = Column(Time, nullable=True)
    venue = Column(String, nullable=True)
    venueId = Column(Integer, nullable=True)
    week = Column(Integer, nullable=False)


def create_db_string():
    # TODO: Return meaningful error if username or pass DNE
    return f'{SQL_DB_TYPE}://{SQL_DB_USERNAME}:{SQL_DB_PASS}@{SQL_SERVICE_HOST}:{SQL_SERVICE_PORT}'


def connect_to_db():
    db = create_engine(create_db_string())
    return db


def check_for_tables():
    """
    Check the db to make sure our desired tables exist
    If they DNE, then create the tables
    """
    engine = connect_to_db()
    m = MetaData()
    m.reflect(engine)

    existing_tables = [table.name for table in m.tables.values()]
    for table in DESIRED_TABLES:
        if table not in existing_tables:
            create_db_tables()


def create_db_tables():
    db = connect_to_db()
    Base.metadata.create_all(db)
