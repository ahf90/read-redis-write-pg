from datetime import datetime, date, time
import json
import os
from prometheus_client import start_http_server, Counter
import redis
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Date, DateTime, Time
from db import connect_to_db, check_for_tables, Game

REDIS_SERVICE_HOST = os.getenv('REDIS_SERVICE_HOST')
REDIS_SERVICE_PORT = os.getenv('REDIS_SERVICE_PORT', 6379)
REDIS_DB_NUM = os.getenv('REDIS_DB_NUM', 0)
REDIS_KEY = os.getenv('REDIS_KEY', 'main')
GAMES_PROCESSED = Counter('games_processed', '# of games ingested by consumer')
GAMES_UPSERTED = Counter('games_upserted', '# of games upserted into db')


def check_env_vars():
    for global_var in [("REDIS_SERVICE_HOST", REDIS_SERVICE_HOST)]:
        var_name, value = global_var[0], global_var[1]
        if not value:
            raise KeyError(f'Missing environment variable: {var_name}')


def connect_to_redis():
    return redis.Redis(host=REDIS_SERVICE_HOST, port=REDIS_SERVICE_PORT, db=REDIS_DB_NUM)


def create_sql_session():
    session = sessionmaker(connect_to_db())
    return session()


def read_from_redis(redis_client):
    return redis_client.blpop(REDIS_KEY)


def parse_data(data):
    return json.loads(data[1].decode())


def get_returned_games(data):
    try:
        return [game for game in parse_data(data)['data']['results']]
    except KeyError:
        return []


def parse_to_dt(column, val):
    """
    Takes string values and formats then in the data type required for DB storage
    :param column: column object
    :param val: value of the cell
    :return: value in correct format
    """
    if not val:
        return val

    if isinstance(column.type, DateTime):
        val = datetime.fromisoformat(val)
    elif isinstance(column.type, Date):
        val = date.fromisoformat(val)
    elif isinstance(column.type, Time):
        val = time.fromisoformat(val)
    return val


def create_game_object(game):
    game_object = Game()
    for column in Game.__table__.columns:
        val = game.get(column.name)
        val = parse_to_dt(column, val)
        setattr(game_object, column.name, val)
    return game_object


def update_row(game, existing_row):
    for column in Game.__table__.columns:
        val = game.get(column.name)
        val = parse_to_dt(column, val)
        if val != getattr(existing_row, column.name):
            setattr(existing_row, column.name, val)


def write_games_to_db(db_session, data):
    """
    This function takes the raw redis data and inserts/updates it into the DB.
    TODO: Consider bulk inserts/updates.  I think given the known, low-scale use case of ~300 writes per minute
        we can forgo bulk operations in favor or more readable, easier-to-debug code
    :param db_session: SQLAlchemy db session object
    :param data: raw data from redis
    :return:
    """
    games = get_returned_games(data)
    GAMES_PROCESSED.inc(len(games))
    for game in games:
        existing_db_object = db_session.query(Game).filter(Game.gameId == game['gameId']).first()

        if existing_db_object:
            update_row(game, existing_db_object)
        else:
            game_object = create_game_object(game)
            db_session.add(game_object)

        if db_session.dirty:
            GAMES_UPSERTED.inc()
            db_session.commit()
    # TODO: Do I need to close the session?


if __name__ == '__main__':
    start_http_server(8000)
    check_env_vars()

    REDIS_CLIENT = connect_to_redis()
    SQL_SESSION = create_sql_session()
    while True:
        check_for_tables()
        REDIS_DATA = read_from_redis(REDIS_CLIENT)
        write_games_to_db(SQL_SESSION, REDIS_DATA)
